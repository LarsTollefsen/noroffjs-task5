$(document).ready(function () {

    //Post Object
    let objPost = (function (userid, postid, posttitle, postbody) {
        let authorid = userid;
        let id = postid;
        let title = posttitle;
        let body = postbody;

        return {
            getAuthorId: () => {
                return authorid;
            },
            getId: () => {
                return id;
            },
            getTitle: () => {
                return title;
            },
            getBody: () => {
                return body;
            }
        }
    });

    //Author Object
    let objAuthor = (function (data) {
        let id = data.id;
        let name = data.name;
        let username = data.username;
        let email = data.email;
        let address = `${data.address.street}, ${data.address.suite} | ${data.address.zipcode} ${data.address.city}`
        let phone = data.phone;
        let website = data.website;
        let company = `${data.company.name} - ${data.company.catchPhrase}`;

        return {
            getId: () => {
                return id;
            },
            getName: () => {
                return name;
            },
            getUsername: () => {
                return username
            },
            getEmail: () => {
                return email;
            },
            getAddress: () => {
                return address;
            },
            getPhone: () => {
                return phone;
            },
            getWebsite: () => {
                return website;
            },
            getCompany: () => {
                return company;
            }
        }
    });

    //Two functions to fetch the url-param
    //If param is chosen, you can chose to make a default value as a return.
    function getUrlVars() {
        var vars = {};
        var parts = window.location.href.replace(/[?&]+([^=&]+)=([^&]*)/gi, function (m, key, value) {
            vars[key] = value;
        });
        return vars;
    }

    function getUrlParam(parameter, defaultvalue) {
        var urlparameter = defaultvalue;
        if (window.location.href.indexOf(parameter) > -1) {
            urlparameter = getUrlVars()[parameter];
        }
        return urlparameter;
    }

    //sleepfunction to make the loading show on modal
    async function sleep(ms) {
        return new Promise(resolve => setTimeout(resolve, ms));
    }

    let loaded = false;

    //fetches the author info to the sidebar
    function getAuthorInfo(userid) {
        $.ajax({
            url: 'https://jsonplaceholder.typicode.com/users/' + userid,
            method: 'GET',

            beforeSend: function (request) {
                console.log("Doing the author request now!", request);
            },
            success: function (data) {
                console.log("Your author fetch was a success!");

                let authorobj = new objAuthor(data);

                //Fills in Author info
                let h2AuthorName = document.createElement("h2");
                h2AuthorName.innerHTML = authorobj.getName();

                let h5Mail = document.createElement("h5");
                h5Mail.innerHTML = authorobj.getEmail();

                let btn = document.createElement("button");
                btn.innerHTML = "More Info";
                btn.setAttribute("type", "button");
                btn.setAttribute("id", authorobj.getId());
                btn.setAttribute("class", "btn btn-sm btn-info btn-lg");
                btn.setAttribute("data-toggle", "modal");
                btn.setAttribute("data-target", "#myModal");
                btn.onclick = function () {
                    getModalAuthorInfo(authorobj.getId())
                };

                $("#divAuthor").append(h2AuthorName, h5Mail, btn);

            }
        });
    }

    //Fetches the url-param for what post was chosen from index
    let postId = getUrlParam("id", "0");

    //Onload instantly fills in postinfo 
    $.ajax({
        url: 'https://jsonplaceholder.typicode.com/posts/' + postId,
        method: 'GET',

        beforeSend: function (request) {
            console.log("Doing the request now!", request);
        },
        success: function (data) {
            console.log("Your post fetch was a success!");

            let postobj = new objPost(data.userId, data.id, data.title, data.body);


            let h2Title = document.createElement("h2");
            h2Title.innerHTML = postobj.getTitle();
            h2Title.setAttribute("class", "capitalize");

            let pBody = document.createElement("p");
            pBody.innerHTML = postobj.getBody();
            pBody.setAttribute("class", "capitalize");

            $("#divPost").append(h2Title, pBody);

            getAuthorInfo(postobj.getAuthorId());
        }
    });

    //Shows modal, asyncronously fetches the data and shows it in the modal when it's finished
    async function getModalAuthorInfo(userid) {
        if (!loaded) {
            $.ajax({
                url: 'https://jsonplaceholder.typicode.com/users/' + userid,
                method: 'GET',

                beforeSend: function (request) {
                    console.log("Doing the author request now!", request);
                },
                success: async function (data) {
                    await sleep(2000);
                    console.log("Your author fetch was a success!");
                    $('#modalSpinner').remove();

                    //Creating the author and adding it to the modal
                    let authorobj = new objAuthor(data);

                    let divModalUl = document.createElement('ul');

                    let divModalUser = document.createElement('li');
                    let divModalEmail = document.createElement('li');
                    let divModalAddress = document.createElement('li');
                    let divModalPhone = document.createElement('li');
                    let divModalWeb = document.createElement('li');
                    let divModalComp = document.createElement('li');

                    //Name
                    $('#modalTitle').html(authorobj.getName());

                    //Username
                    let pUser = document.createElement('p');
                    let iUser = document.createElement('i');

                    iUser.setAttribute("class", "fa fa-user-circle");
                    pUser.append(iUser);
                    pUser.innerHTML += ' ' + authorobj.getUsername();
                    divModalUser.append(pUser);

                    //Email
                    let pEmail = document.createElement('p');
                    let iEmail = document.createElement('i');

                    iEmail.setAttribute("class", "fa fa-envelope");
                    pEmail.append(iEmail);
                    pEmail.innerHTML += ' ' + authorobj.getEmail();
                    divModalEmail.append(pEmail);

                    //Address
                    let pAddress = document.createElement('p');
                    let iAddress = document.createElement('i');

                    iAddress.setAttribute("class", "fa fa-home");
                    pAddress.append(iAddress);
                    pAddress.innerHTML += ' ' + authorobj.getAddress();
                    divModalAddress.append(pAddress);

                    //Phone
                    let pPhone = document.createElement('p');
                    let iPhone = document.createElement('i');

                    iPhone.setAttribute("class", "fa fa-phone");
                    pPhone.append(iPhone);
                    pPhone.innerHTML += ' ' + authorobj.getPhone();
                    divModalPhone.append(pPhone);

                    //Website
                    let pWeb = document.createElement('p');
                    let iWeb = document.createElement('i');

                    iWeb.setAttribute("class", "fa fa-globe");
                    pWeb.append(iWeb);
                    pWeb.innerHTML += ' ' + authorobj.getWebsite();
                    divModalWeb.append(pWeb);

                    //Company
                    let pComp = document.createElement('p');
                    let iComp = document.createElement('i');

                    iComp.setAttribute("class", "fa fa-building");
                    pComp.append(iComp);
                    pComp.innerHTML += ' ' + authorobj.getCompany();
                    divModalComp.append(pComp);

                    //Appends all
                    divModalUl.append(divModalUser, divModalEmail, divModalAddress, divModalPhone, divModalWeb, divModalComp);

                    $('#modalBody').append(divModalUl);

                    //Sets the load to true so it doesn't fetch it twice if you close the modal and open it again.
                    loaded = true;
                }
            });
        }

    }
});