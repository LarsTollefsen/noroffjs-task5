$(document).ready(function () {
    // Fetches all posts and shows them in a list
    $.ajax({
        url: 'https://jsonplaceholder.typicode.com/posts',
        method: 'GET',

        beforeSend: function (request) {
            console.log("Doing the request now!", request);
        },
        success: function (data) {
            console.log("Your posts fetch was a success!");

            $.each(data, function (id, title) {
                let listobj = document.createElement("a");
                listobj.setAttribute("class", "list-group-item");
                listobj.setAttribute("id", this.id);
                listobj.innerHTML = this.title;
                listobj.setAttribute("href", `post.html?id=${this.id}`);
                // Links to post with id param on what link was clicked

                $("#postlist").append(listobj);
            });

        }
    });
});